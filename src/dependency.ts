import * as fs from 'fs';
import * as path from 'path';
import * as ylockfile from '@yarnpkg/lockfile';


const YARNLOCKFILEBASENAME = 'yarn.lock';



export interface INodeJsDependencyResolver {
    readonly rootpath: string;
}



export function _inArray( needle:any, haystack:any[] ) {

    for(var i = 0; i < haystack.length; i += 1 ) {

        if( haystack[i] == needle ) return true;
    }

    return false;
}



export function _loadYarnLockFile( 
    basepath: string, 
    basename?:string
): ylockfile.LockFileObject {

    basename = basename ?? YARNLOCKFILEBASENAME;

    var lockFilePath =  path.join( basepath, basename );

    if ( ! fs.existsSync( lockFilePath ) ) {

        throw new Error( `yarn lockfile '${lockFilePath}' does not exist.` );
    }

    var fileObj = fs.readFileSync( lockFilePath, 'utf8' );

    return ylockfile.parse( fileObj ).object;
}



export function _moduleDefinedInYarnLockfile( 
    name:string, 
    lockFileObj: ylockfile.LockFileObject
): string|undefined {

    var moduleMatch:string|undefined = undefined;

    var moduleNames:string[] = Object.keys( lockFileObj );

    for ( var i = 0; i < moduleNames.length; i += 1 ) {

        let key = moduleNames[ i ];

        if ( key.startsWith( name ) ) {

            //make sure we get an exact match
            if ( key.length > name.length && key[ name.length ] != '@' ) {

                continue
            }

            moduleMatch = key;

            break;
        }
    }

    return moduleMatch;
}



export function _resolveYarnLockFileDependency( 
    name:string,
    lockfile:ylockfile.LockFileObject,
    pedigree?: string[]
): string[] {

    pedigree = pedigree ?? [];

    var dependencies:string[] = [];

    var moduleMatch:string|undefined = _moduleDefinedInYarnLockfile( 
        name,
        lockfile
    );

    if ( moduleMatch == undefined ) {

        let msg = `module '${name}' not defined in lock file.`;

        throw new Error( msg );
    }

    dependencies.push( moduleMatch );
    pedigree.push( moduleMatch );

    var dependencyModuleNames = Object.keys( 
        lockfile[ moduleMatch ].dependencies!
    );

    for ( var i = 0; i < dependencyModuleNames.length; i += 1 ) {

        let key = dependencyModuleNames[ i ];

        let nestedDependencies = _resolveYarnLockFileDependency( 
            key,
            lockfile,
            pedigree
        );

        for ( var j = 0; j < nestedDependencies.length; j += 1 ) {

            let ndep = nestedDependencies[ j ];

            if ( ! _inArray( ndep, dependencies ) ) {

                dependencies.push( ndep );
            }
        }
    }

    return dependencies;
}



/**
 * Yarn dependency archive base class
 *
 */
export class YarnDependencyResolver implements INodeJsDependencyResolver {

    readonly rootpath: string;
    readonly lockfile: ylockfile.LockFileObject;

    /**
     * mark files for archiving through a glob pattern
     *
     * @param    {String} rootpath        node module base directory
    **/
    constructor( rootpath: string ) {

        this.rootpath = path.resolve( rootpath );

        this.lockfile = _loadYarnLockFile( this.rootpath );
    }




    dump() {

    }
}