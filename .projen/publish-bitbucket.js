function _bitbucketPublishCallback() {

  'use strict';
  const fs = require( 'fs' );
  const https = require( 'https' );
  const path = require( 'path' );


  /**
         * Get filesystem metadata of a `yarn pack` build archive
         *
         *
         */
  function _getDistributionFsMeta( cwd ) {

    cwd = cwd ?? process.cwd();

    var packageManifest = require( path.join( cwd, 'package.json' ) );

    var _packageNameSegments = packageManifest.name.split( '/' );

    var packageScope = undefined;
    if ( _packageNameSegments.length > 1 ) {

      packageScope = _packageNameSegments[ 0 ].substring( 1 );

      _packageNameSegments.shift();
    }

    var packageName = _packageNameSegments.join();

    var packageVersion = packageManifest.version;

    var distBaseName = `${packageName}-v${packageVersion}.tgz`;

    distBaseName = ( packageScope !== undefined )
      ? `${packageScope}-${distBaseName}` : distFileName;

    var distRelPath = `dist/js/${distBaseName}`;

    return {
      basename: distBaseName,
      relpath: distRelPath,
    };
  }


  /**
         * Upload a single file to a Bitbucket repository artifact store
         */
  function postBitbucketRepositoryDownload( filePath, uname, pwd, owner, slug ) {

    const BITBUCKET_USERNAME = uname;
    const BITBUCKET_APP_PASSWORD = pwd;
    const BITBUCKET_REPO_OWNER = owner;
    const BITBUCKET_REPO_SLUG = slug;

    if ( ! fs.existsSync( filePath ) ) {

      throw new Error( `${filePath} does not exist.` );
    }

    var basename = path.basename( filePath );

    var boundary = '------------------------2508c475d9f43373';

    var data = `--${boundary}\r\n`;
    data += 'Content-Disposition: form-data; name="files"; ';
    data += `filename="${basename}"\r\n`;
    data += 'Content-Type: application/octet-stream\r\n\r\n';
    data += fs.readFileSync( filePath ).toString( 'utf-8' );
    data += `\r\n--${boundary}--\r\n`;

    var requestOptions = {
      hostname: 'api.bitbucket.org',
      port: 443,
      path: `/2.0/repositories/${BITBUCKET_REPO_OWNER}/${BITBUCKET_REPO_SLUG}/downloads`,
      method: 'POST',
      headers: {
        'Authorization': 'Basic ' + Buffer.from( `${BITBUCKET_USERNAME}:${BITBUCKET_APP_PASSWORD}` ).toString( 'base64' ),
        'Content-Type': `multipart/form-data; boundary=${boundary}`,
        'Content-Length': data.length,
      },
    };

    var request = https.request( requestOptions, ( response ) => {

      if ( ! response.statusCode.toString().startsWith( '2' ) ) {

        response.on('data', function ( chunk ) {

          process.stdout.write( chunk );
        });

        response.on( 'end', () => {

          var msg = `HTTP[${response.statusCode}]:`;
          msg += `${requestOptions.hostname}${requestOptions.path}`;

          throw new Error( msg );
        });
      }
    } );

    request.write( data, 'binary' ) ;

    request.end();
  }


  if ( require.main === module ) {

    var meta = _getDistributionFsMeta();

    postBitbucketRepositoryDownload(
      meta.relpath,
      process.env.BITBUCKET_USERNAME,
      process.env.BITBUCKET_APP_PASSWORD,
      process.env.BITBUCKET_REPO_OWNER,
      process.env.BITBUCKET_REPO_SLUG,
    );
  }
}
_bitbucketPublishCallback();