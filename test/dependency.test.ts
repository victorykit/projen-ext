import * as dependency from '../src/dependency';



describe( '_moduleDefinedInYarnLockfile', () => {

    test( 'default (unscoped)', () => {

        var lockfile = {
            'bar@^1.0.0': {
                version: '1.0.0',
                resolved: 'https://registry.foo.bar/foo/bar?v=1.0.0',
                dependencies: {
                    '@foo/foo': '^2.0.0'
                }
            }
        }

        var result = dependency._moduleDefinedInYarnLockfile( 'bar', lockfile );

        expect( result ).toBe( 'bar@^1.0.0' );
    } );


    test( 'default (scoped)', () => {

        var lockfile = {
            '@foo/bar@^1.0.0': {
                version: '1.0.0',
                resolved: 'https://registry.foo.bar/foo/bar?v=1.0.0',
                dependencies: {
                    '@foo/foo': '^2.0.0'
                }
            }
        }

        var result = dependency._moduleDefinedInYarnLockfile( '@foo/bar', lockfile );

        expect( result ).toBe( '@foo/bar@^1.0.0' );
    } );


    test( 'exact match', () => {

        var lockfile = {
            '@foo/barfoo@^1.0.0': {
                version: '1.0.0',
                resolved: 'https://registry.foo.bar/foo/barfoo?v=1.0.0',
                dependencies: {
                    '@foo/foo': '^2.0.0'
                }
            },
            '@foo/bar@^1.0.0': {
                version: '1.0.0',
                resolved: 'https://registry.foo.bar/foo/bar?v=1.0.0',
                dependencies: {
                    '@foo/foo': '^2.0.0'
                }
            }
        }

        var result = dependency._moduleDefinedInYarnLockfile( '@foo/bar', lockfile );

        expect( result ).toBe( '@foo/bar@^1.0.0' );
    } );


    test('mismatch', () => {

        var lockfile = {}

        var result = dependency._moduleDefinedInYarnLockfile( '@foo/bar', lockfile );

        expect( result ).toBe( undefined );
    } );
} );



describe('_resolveYarnLockFileDependency', () => {

    test('default', () => {

        var lockfile = {
            '@foo/bar@^1.0.0': {
                version: '1.0.0',
                resolved: 'https://registry.foo.bar/foo/bar?v=1.0.0',
                dependencies: {}
            }
        }

        var result = dependency._resolveYarnLockFileDependency( '@foo/bar', lockfile );

        expect( result ).toStrictEqual( [
            '@foo/bar@^1.0.0',
        ] );
    } );


    test( 'nested (level 1)', () => {

        var lockfile = {
            '@foo/bar@^1.0.0': {
                version: '1.0.0',
                resolved: 'https://registry.foo.bar/foo/bar?v=1.0.0',
                dependencies: {
                    '@bar/foo': '^2.0.0'
                }
            },
            '@bar/foo@^1.0.0': {
                version: '1.0.0',
                resolved: 'https://registry.foo.bar/bar/foo?v=1.0.0',
                dependencies: {}
            }
        }

        var result = dependency._resolveYarnLockFileDependency( '@foo/bar', lockfile );

        expect( result ).toStrictEqual( [
            '@foo/bar@^1.0.0',
            '@bar/foo@^1.0.0'
        ] );
    } );


    test( 'nested (level 2)', () => {

        var lockfile = {
            '@foo/bar@^1.0.0': {
                version: '1.0.0',
                resolved: 'https://registry.foo.bar/foo/bar?v=1.0.0',
                dependencies: {
                    '@bar/foo': '^2.0.0'
                }
            },
            '@bar/foo@^1.0.0': {
                version: '1.0.0',
                resolved: 'https://registry.foo.bar/bar/foo?v=1.0.0',
                dependencies: {
                    'foobar': '^1.0.0'
                }
            },
            'foobar@^1.0.0': {
                version: '1.0.0',
                resolved: 'https://registry.foo.bar/bar/foo?v=1.0.0',
                dependencies: {}
            }
        }

        var result = dependency._resolveYarnLockFileDependency( '@foo/bar', lockfile );

        expect( result ).toStrictEqual( [
            '@foo/bar@^1.0.0',
            '@bar/foo@^1.0.0',
            'foobar@^1.0.0'
        ] );
    } );
} );
